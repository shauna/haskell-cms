# haskell-cms

## Goals

To make a better CMS.

- Solid plugin interface
- Solid public API
- Control over routes
- Easily themable

## Todo

- [ ] Routing override framework
    - [ ] Default routing
- [ ] Storage
- [ ] Module/Extension Framework
- [ ] Admin
    - [ ] User management
    - [ ] Access control
- [ ] Modules
    - [ ] Pages
        - [ ] Define page types (blog, page, etc)
    - [ ] Front-end core
        - [ ] Default front-end
